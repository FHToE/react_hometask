import TextareaAutosize from 'react-textarea-autosize';
import React, { Component } from 'react';
import styles from './styles.module.scss';
import { Popup, Label, Icon, Button } from 'semantic-ui-react';

class EditMessage extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: ""
        }
        this.handleEdit =  this.handleEdit.bind(this);
    }

    handleEdit() {
        const message = {
            id: this.props.id,
            userId: this.props.userId,
            avatar: this.props.avatar,
            user: this.props.user,
            text: this.state.text,
            createdAt: this.props.createdAt,
            editedAt: Date.now()
        }
        this.props.edit(message);
        document.body.click();
    }

    setText(changed) {
        this.setState({text: changed})
    }

    componentDidMount() {
        this.setState({ text: this.props.text });
    }
    
    render(){
        const data = this.state;

        const FieldForEdit = () => (
            <div>
              <TextareaAutosize
                rows="2"
                style={{ wordWrap: 'break-word', width: "300px" }}
                autoFocus
                defaultValue={data.text}
                onChange={e => this.setText(e.currentTarget.value)}
              />
              <br />
              <Button
                compact
                color="blue"
                size="mini"
                disabled={this.props.text === this.state.text || this.state.text.trim().length === 0}
                onClick={this.handleEdit}
              >
                Save
              </Button>
            </div>
          );
          return (
            <Popup
              trigger={(
                <Label attached="bottom right" basic size="tiny" as="a" className={styles.toolbarBtn}>
                  <Icon color="blue" name="pencil alternate">
                    {'\u2007\u2007\u2007\u2007\u2007'}
                  </Icon>
                </Label>
              )}
              on="click"
              position='bottom right'
            >
              {FieldForEdit()}
            </Popup>
          );
    }
    
}

export default EditMessage;