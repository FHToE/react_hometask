import React, { Component } from 'react';
import { Segment, Form, Button } from 'semantic-ui-react';

class Sender extends Component {
    constructor(props){
        super(props);
        this.state = {
            messageText: ''
        }
       this.handleSend =  this.handleSend.bind(this);
    }

    setText(e) {
        this.setState({messageText: e});
    }

    handleSend() {
        if (this.state.messageText === '') return;
        const message = {
            id: ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            ),
            userId: this.props.userId,
            avatar: null,
            user: "NickName",
            text: this.state.messageText,
            createdAt: Date.now(),
            editedAt: ""
        }
        this.props.send(message);
        this.setState({messageText: ''});
    }
    
    render() {
        const data = this.state;
        return (
            <Segment style={ { paddingBottom: "50px" ,background: "rgb(181, 200, 207)" }}>
                <Form onSubmit={this.handleSend}>
                    <Form.TextArea
                        name="body"
                        placeholder="Your message..."
                        value={data.messageText}
                        onChange={ev => this.setText(ev.target.value)}
                    />
                    <Button floated="right" color="blue" type="submit">Send</Button>
                </Form>
            </Segment>
        )
    }
}

export default Sender;