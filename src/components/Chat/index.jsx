import styles from './styles.module.scss';
import React, { Component } from 'react';
import { Loader, Grid, Segment } from 'semantic-ui-react';
import Message from '../Message';
import Sender from '../Sender';
import UserDrop from '../UsersDrop';

class Chat extends Component {
    constructor(props){
        super(props);
        this.state = {
            messages: [],
            isLoaded: false,
            lastMsgDate: null,
            users: [],
            currentUserId: null,
            filterId: null
        }
        this.handleSend =  this.handleSend.bind(this);
        this.handleEdit =  this.handleEdit.bind(this);
        this.handleRemove =  this.handleRemove.bind(this);
        this.handleFilter =  this.handleFilter.bind(this);
    }
    uuidv4() {
        return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
        );
    }

    handleSend(msg) {
        const messageList = [...this.state.messages, msg];
        this.setState({messages: messageList})
    }

    handleEdit(msg) {
        const messageList = [...this.state.messages];
        const index = messageList.map(m=>m.id).indexOf(msg.id);
        if (index !== -1) {
            messageList[index] = msg;
        }
        this.setState({messages: messageList})
    }

    handleRemove(id) {
        const messageList = [...this.state.messages];
        const index = messageList.map(m=>m.id).indexOf(id);
        if (index !== -1) {
            messageList.splice(index,1);
        }
        this.setState({messages: messageList});
    }

    handleFilter(id) {
        this.setState({filterId: id});
    }

    componentDidUpdate() {
        const msgs = this.state.messages;
        const last = msgs[msgs.length-1];
        const date = new Date(last.createdAt);
        const hours = date.getHours().toString().length === 2? date.getHours().toString() : "0" + date.getHours().toString();
        const minutes = date.getMinutes().toString().length === 2? date.getMinutes().toString() : "0" + date.getMinutes().toString();
        const time = "" + hours + ":" + minutes;
        const usersList = [...this.state.users];
        if (usersList.find(x => x.id === last.userId) === undefined) {
            usersList.push({
                key: last.userId,
                text: last.user,
                id: last.userId,
                image: { 
                    avatar: true, 
                    src: last.avatar===null? "https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png" : last.avatar
                }
            })
            this.setState({users: usersList});
        }
        if (this.state.lastMsgDate !== time) {
            this.setState({lastMsgDate: time});
        }
        
        const scrolling = document.getElementById("scroller");
        scrolling.scrollTop = scrolling.scrollHeight+1;
    }

    componentDidMount() {
        
        fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
          .then(response => response.json())
          .then(messages => {
                const uniqUsersId = [...new Set(messages.map(m=>m.userId))];
                const users = [];
                uniqUsersId.forEach(uniq => {
                    const message = messages.find(x => x.userId === uniq);
                    users.push({
                        key: message.userId,
                        text: message.user,
                        id: message.userId,
                        image: { 
                            avatar: true, 
                            src: message.avatar===null? "https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png" : message.avatar
                        }
                    });
                })

                const mcount = messages.length;
                const time = "" + new Date(messages[mcount-1].createdAt).getHours()+':'+ new Date(messages[mcount-1].createdAt).getMinutes();
                const currentUserId = this.uuidv4();
                const sorted = messages.sort((m1, m2) => (new Date(m1.createdAt)-new Date(m2.createdAt)));
                this.setState({ messages: sorted, isLoaded: true, lastMsgDate: time, users, currentUserId });
                
                const scrolling = document.getElementById("scroller");
                scrolling.scrollTop = scrolling.scrollHeight;
            });            
      }
    

    render() {
        const data = this.state;
        var dateContainer = 0;
        function isFirst(date) {
            const day = new Date(date).getDay();
            if (day !== dateContainer) {
                dateContainer = day;
                return true; 
            }
            return false;
        }
        return(
        <div>
            <div><img alt="bsa" src={require('./academy.png')}  className={styles.logo}></img></div>
            <div className={styles.content}>
                
                {!data.isLoaded? <Loader active inline="centered" /> : 
                <Grid className={styles.header} centered container columns="12">
                    <Grid.Row centered verticalAlign="middle" color="blue" >
                        <Grid.Column className="chat" width="2" textAlign="center" floated="left">
                            My chat:)
                        </Grid.Column>
                        <Grid.Column className={styles.users} width="5" textAlign="left" floated="left">
                                <Grid.Row>
                                    {data.users.length} participants
                                </Grid.Row>
                                <Grid.Row>
                                    <UserDrop handleFilter = {this.handleFilter} users = {data.users} />
                                </Grid.Row>
                        </Grid.Column>
                        <Grid.Column width="2" textAlign="left" floated="left">
                            {data.messages.length} messages
                        </Grid.Column>
                        <Grid.Column className="lastMsg" width="4" textAlign="center" floated="right">
                            Last message at {data.lastMsgDate}
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                }
                {!data.isLoaded? "" : 
                <Segment id="scroller" raised style={ {overflow: "auto", height: "27em", background: "rgb(181, 200, 207)" }}>
                    {data.messages.map(mes=> {
                        if (data.filterId === null || data.filterId === mes.userId) return (
                            <Message 
                            isFirst = {isFirst(mes.createdAt)}
                            key =  {mes.id}
                            id = {mes.id}
                            user = {mes.user}
                            avatar = {mes.avatar}
                            createdAt = {mes.createdAt}
                            text = {mes.text}
                            userId = {mes.userId}
                            currentUserId = {this.state.currentUserId}
                            edit = {this.handleEdit}
                            remove = {this.handleRemove}
                        />)
                        return "";
                    } )
                        }
                </Segment>
                }
                {!data.isLoaded? "" : 
                <Sender userId={data.currentUserId} send={this.handleSend}/>
            }
            </div>
        </div>
        ); 
    }
};

export default Chat;
