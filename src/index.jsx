import React from 'react';
import { render } from 'react-dom';
import Chat from './components/Chat';
import 'semantic-ui-css/semantic.min.css';
import './styles/common.scss';

const target = document.getElementById('root');
render(<Chat />, target);
